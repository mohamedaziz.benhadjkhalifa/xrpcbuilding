using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GrabDetection : MonoBehaviour
{
    // Public GameObject that you can assign in the Inspector
    public GameObject targetGameObject;

    // This method is called when another collider enters the trigger collider attached to the object where this script is attached.
    private void OnTriggerEnter(Collider other)
    {
        // Check if the collider that entered the trigger has the layer "Water"
        if (other.gameObject.layer == LayerMask.NameToLayer("Water"))
        {
            // Log "Hello there" to the console
            Debug.Log("Hello there");

            // Disable the target GameObject
            if (targetGameObject != null)
            {
                targetGameObject.SetActive(false);
            }
        }
    }

    // This method is called when another collider exits the trigger collider attached to the object where this script is attached.
    private void OnTriggerExit(Collider other)
    {
        // Check if the collider that exited the trigger has the layer "Water"
        if (other.gameObject.layer == LayerMask.NameToLayer("Water"))
        {
            // Enable the target GameObject
            if (targetGameObject != null)
            {
                targetGameObject.SetActive(true);
            }
        }
    }
}
