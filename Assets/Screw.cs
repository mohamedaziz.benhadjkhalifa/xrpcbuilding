using Oculus.Interaction;
using Oculus.Platform.Models;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Device;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class Screw : MonoBehaviour
{
    public Transform screwHole;

    public bool isAttached = false;
    public bool isScrued = false;
    private Rigidbody rb;

    public float transitionSpeed = 1.0f;

    public bool screwing = false;

    public bool grabbable = true;

    public bool isGrabbed = false;

    public float moveSpeed = 1.0f; // Speed of movement
    public float rotationSpeed = 100.0f; // Speed of rotation


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    private void Update()
    {

            if (isScrued)
            {
                grabbable = false;
            }
            if (!isScrued)
            {
                grabbable = true;
            }


            ////////////////toggle the grabbable based on grabbable state 
            var grabbableComponent = GetComponent<GrabInteractable>();
        
            if (grabbableComponent != null)
            {
                // Activate or deactivate the component based on the value of grabbable
                grabbableComponent.enabled = grabbable;
                Debug.Log($"Grabbable component is now {(grabbable ? "enabled" : "disabled")}");
            }
            else
            {
                Debug.LogWarning("Grabbable component not found!");
            }

        //  }

            //////////isgrabbed verefy 
        IsGrabbed isGrabbedComponent = GetComponent<IsGrabbed>();
        if (isGrabbedComponent != null)
        {
            if (isGrabbedComponent.isGrabbed) 
            {
                isGrabbed = true;
            }
            if (!isGrabbedComponent.isGrabbed)
            {
                isGrabbed = false;
            }

        }
        if (isGrabbed) 
                {
            isAttached = false;
            transform.parent = null;
            rb.isKinematic = true; 
                 }
        if (isAttached || isScrued)
        {
            rb.isKinematic = true;
        }
        if(isGrabbed || !isAttached) {
            rb.isKinematic = false;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("screwHole") && !isGrabbed)
        {
            Transform screwHoleParent = other.transform.parent;


                if (!isAttached)
                {
                //    isAttached = true;   //newly added ������������!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! B E A W A R E ! �! ! !
                    if (rb != null)
                    {
                        rb.isKinematic = true;
                    }
                    // Set CapsuleCollider to trigger only if it's not already a trigger
                    if (GetComponent<CapsuleCollider>().isTrigger == false)
                    {
                        GetComponent<CapsuleCollider>().isTrigger = true;
                    }

                    screwHole = other.transform;
                transform.parent = screwHoleParent;
               Transform screwPos1 = screwHoleParent.Find("screwPos1");

                transform.position = screwPos1.position;
                transform.rotation = screwPos1.rotation;



                isAttached = true;


                


                Debug.Log("Screw attached successfully");
                // StartCoroutine(SnapToScrewTipCoroutine(screwHole));

            }
            

        }
        if (other.CompareTag("ScrewDriver") && isAttached && !screwing)
        {
            Transform holeParent = screwHole.parent;
            Transform BigParent = holeParent.parent;

            Transform screwPos1 = holeParent.Find("screwPos1");
            Transform screwPos2 = holeParent.Find("screwPos2");


            if (BigParent.tag == "motherboard")
            {
                Motherboard1 motherboardScript = BigParent.GetComponent<Motherboard1>();

                if (isScrued)
                {
                    motherboardScript.nbScrew--;
                    //  motherboardScript.scrued = false;
                    StartCoroutine(MoveAndRotateToPositionB(screwPos1));


                    // StartCoroutine(screwCouroutineB(screwHole));
                }
                else if (!isScrued)
                {
                    motherboardScript.nbScrew++;
                    StartCoroutine(MoveAndRotateToPosition(screwPos2));
                    //motherboardScript.scrued = true;
                    // StartCoroutine(screwCouroutine(screwHole));
                }
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("screwHole") && isGrabbed)
        {
            Debug.Log("trigger exit screw");
            if (isGrabbed)
            {
                if (rb != null)
                {
                    rb.isKinematic = false;
                }
                // Set CapsuleCollider to trigger only if it's not already a trigger

                GetComponent<CapsuleCollider>().isTrigger = false;
            }
          

        }
       

         //  isAttached = false;




        // transform.parent = null;


    }



    IEnumerator SnapToScrewTipCoroutine(Transform screwHole)
    {
        Debug.Log("screwtip found!");

        // Optionally, you can also disable physics if needed





        // Find the desired child (screwTip)
        Transform screwTip = transform.Find("screwTip");

        // Ensure the screw aligns with the screw hole in X and Z rotation
        Quaternion targetRotation = Quaternion.Euler(screwHole.eulerAngles.x, screwTip.eulerAngles.y, screwHole.eulerAngles.z);

        // Calculate target position in X and Z
        while ((Quaternion.Angle(transform.rotation, targetRotation) > 0.001f))
        {
            // Combine rotation and position adjustments
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, transitionSpeed * Time.deltaTime * 100);
            Debug.Log("screwtip zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz!");

            yield return null;
        }

        // Calculate target position in X and Z
        Vector3 screwTipWorldPosition = screwTip.position;
        Vector3 offset = screwTipWorldPosition - transform.position;
        Vector3 targetPositionXZ = new Vector3(screwHole.position.x - offset.x, transform.position.y, screwHole.position.z - offset.z);

        // Move in X and Z first
        while (Mathf.Abs(screwTip.position.x - screwHole.position.x) > 0.001f || Mathf.Abs(screwTip.position.z - screwHole.position.z) > 0.001f)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPositionXZ, transitionSpeed * Time.deltaTime);
            yield return null;
        }

        // Calculate target position in Y
        Vector3 targetPositionY = new Vector3(transform.position.x, screwHole.position.y - offset.y, transform.position.z);

        // Move in Y after X and Z alignment
        while (Mathf.Abs(screwTip.position.y - screwHole.position.y) > 0.001f)
        {
            Debug.Log("attaching");
            transform.position = Vector3.MoveTowards(transform.position, targetPositionY, transitionSpeed * Time.deltaTime);
            transform.Rotate(Vector3.up, 4 * transitionSpeed * Time.deltaTime * 500);
            yield return null;
        }



        isAttached = true;
        

        Transform screwHoleParent = screwHole.parent;

        transform.parent = screwHoleParent;

        Debug.Log("Screw attached successfully");
    }


    IEnumerator screwCouroutine(Transform screwHole)
    {
        screwing = true;
        Transform screwHP = transform.Find("screwHP");

        Transform parentTransform = transform.parent;
        Transform screwSP = parentTransform.Find("screwSP");

        if (screwHP == null)
        {
            Debug.Log("screwHP NULL");
        }

        if (screwSP == null)
        {
            Debug.Log("screwSP NULL");
        }
        // Calculate target position in X and Z
        Vector3 screwTipWorldPosition = screwHP.position;
        Vector3 offset = screwTipWorldPosition - transform.position;

        // Calculate target position in Y
        Vector3 targetPositionY = new Vector3(transform.position.x, screwSP.position.y - offset.y, transform.position.z);

        while (Mathf.Abs(screwHP.position.y - screwSP.position.y) > 0.001f)
        {
            // Calculate rotation to face the target position
            Vector3 direction = (targetPositionY - transform.position).normalized;
            Quaternion targetRotation = Quaternion.LookRotation(direction);

            // Move towards the target position
            transform.position = Vector3.MoveTowards(transform.position, targetPositionY, transitionSpeed * Time.deltaTime);

            // Rotate towards the target rotation
            transform.Rotate(Vector3.up, 4 * transitionSpeed * Time.deltaTime * 500);
            yield return null;
        }

        screwing = false;
        isScrued = true;

        Debug.Log("screw scrued");
    }

    IEnumerator screwCouroutineB(Transform screwHole)
    {
        screwing = true;
        // Find the desired child (screwTip)
        Transform screwTip = transform.Find("screwTip");

        Transform parentTransform = transform.parent;
        screwHole = parentTransform.Find("screwDetecter");

        if (screwTip == null)
        {
            Debug.Log("screwTip NULL");
        }

        if (screwHole == null)
        {
            Debug.Log("screwHole NULL");
        }
        // Calculate target position in X and Z
        Vector3 screwTipWorldPosition = screwTip.position;
        Vector3 offset = screwTipWorldPosition - transform.position;

        // Calculate target position in Y
        Vector3 targetPositionY = new Vector3(transform.position.x, screwHole.position.y - offset.y, transform.position.z);

        while (Mathf.Abs(screwTip.position.y - screwHole.position.y) > 0.001f)
        {
            // Calculate rotation to face the target position
            Vector3 direction = (targetPositionY - transform.position).normalized;
            Quaternion targetRotation = Quaternion.LookRotation(direction);

            // Move towards the target position
            transform.position = Vector3.MoveTowards(transform.position, targetPositionY, transitionSpeed * Time.deltaTime);

            // Rotate towards the target rotation
            transform.Rotate(Vector3.up, -4 * transitionSpeed * Time.deltaTime * 500);
            yield return null;
        }
        screwing = false;
        isScrued = false;
        Debug.Log("screw unscrued");
    }
    private IEnumerator MoveAndRotateToPosition(Transform target)
    {
        screwing = true;
        Vector3 startPosition = transform.position;
        Vector3 targetPosition = target.position;
        float distance = Vector3.Distance(startPosition, targetPosition);
        float startTime = Time.time;

        while (distance > 0.1f) // Continue until close enough to target
        {
            // Calculate the fraction of journey completed
            float journeyLength = distance;
            float distCovered = (Time.time - startTime) * moveSpeed;
            float fractionOfJourney = distCovered / journeyLength;

            // Move the object to the target position
            transform.position = Vector3.Lerp(startPosition, targetPosition, fractionOfJourney);

            // Rotate the object around its y-axis
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);

            // Recalculate the distance
            distance = Vector3.Distance(transform.position, targetPosition);

            yield return null; // Wait for the next frame
        }

        // Ensure the object is exactly at the target position
        transform.position = targetPosition;

        screwing = false;
        isScrued = true;
    }

    private IEnumerator MoveAndRotateToPositionB(Transform target)
    {
        screwing = true;
        Vector3 startPosition = transform.position;
        Vector3 targetPosition = target.position;
        float distance = Vector3.Distance(startPosition, targetPosition);
        float startTime = Time.time;

        while (distance > 0.1f) // Continue until close enough to target
        {
            // Calculate the fraction of journey completed
            float journeyLength = distance;
            float distCovered = (Time.time - startTime) * moveSpeed;
            float fractionOfJourney = distCovered / journeyLength;

            // Move the object to the target position
            transform.position = Vector3.Lerp(startPosition, targetPosition, fractionOfJourney);

            // Rotate the object around its y-axis
            transform.Rotate(-Vector3.up, rotationSpeed * Time.deltaTime);

            // Recalculate the distance
            distance = Vector3.Distance(transform.position, targetPosition);

            yield return null; // Wait for the next frame
        }

        // Ensure the object is exactly at the target position
        transform.position = targetPosition;

        screwing = false;
        isScrued = false;
    }


}
