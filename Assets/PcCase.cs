using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PcCase : MonoBehaviour
{
    private bool motherboardConnected = false;

    public GameObject connectedMotherboard = null;

    


    public bool motherScrew = false;
    public bool isGrabbed = false;


    private void OnCollisionStay(Collision collision)
    {
      //  Debug.Log("Collision detected with: " + collision.gameObject.name);
        // Check if the colliding object has the "Motherboard" tag


        if (collision.gameObject.CompareTag("motherboard") )
        {
          //  Debug.Log(" motherboard detecteed from case  ");
            // Set motherboardConnected to true
            if (!motherScrew)
            {
                Motherboard ms = collision.gameObject.GetComponent<Motherboard>();
                if (ms.motherboardPlaced && !ms.isgrabbed)     /////qdded condition of matherboard is grabbed newly
                {
                    if (ms.nbScrew == 0)
                    {
                        motherboardConnected = true;
                        // Store the connected motherboard
                        connectedMotherboard = collision.gameObject;
                    }
                    if (ms.nbScrew != 0)
                    {
                    //    Debug.Log(" unscrew first please   ");////////////////////UI
                    }

                }

            }

        }
    }

    private void OnCollisionExit(Collision collision)
    {
        // Check if colliding object has the "Motherboard" tag
        if (collision.gameObject.CompareTag("motherboard") )
        {
            Motherboard ms = collision.gameObject.GetComponent<Motherboard>(); // added recentely
            if (ms.isgrabbed)
            {
           //     Debug.Log("Motherboard disconnected from case");
                motherboardConnected = false;
            }
           
        }
    }






    private void Update()
    {

        if (connectedMotherboard != null)
        {

            Motherboard motherboardScript = connectedMotherboard.GetComponent<Motherboard>();

            if (motherboardScript.nbScrew > 0)
            {
                motherScrew = true;

                connectedMotherboard.transform.SetParent(transform);
                motherboardConnected = true;
                motherboardScript.grabbable = false;
                motherboardScript.installed = true;


            }
            if (motherboardScript.nbScrew == 0)
            {
                if (motherScrew)
                {
                    motherboardScript.grabbable = true;
                    motherboardScript.installed = false;
                    connectedMotherboard.transform.SetParent(null);
                    connectedMotherboard = null;
                    motherScrew = false;
                    Debug.Log("motherscrew , set connected motherboard to null ");

                }

            }
        }
    }
}
