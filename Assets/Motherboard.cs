using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Motherboard : MonoBehaviour
{
    public bool grabbable = true;
    private Transform motherboardDetect;
    public bool rotationLimit = false;
    //public  bool noScrew = true;
    public bool motherboardPlaced = false;

    public bool installed = false;
    public bool isgrabbed = false;

   
    public int nbScrew = 0;

    void Update()
    {
        if (installed)
        {
            grabbable = false;
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
        }
        if (!installed ) // zedtha jdida !isgrabebd  condition fi west l condition  
        {
            grabbable = true;
            Rigidbody rb = GetComponent<Rigidbody>();  // tetbqdel fi ontrigger exit , suspessious ////////////////////*//////////////////////*/////////////////////////////////////Alam
            rb.isKinematic = false;
        }



        ////////////////toggle the grabbable based on grabbable state 
        var grabbableComponent = GetComponent<GrabInteractable>();

        if (grabbableComponent != null)
        {
            // Activate or deactivate the component based on the value of grabbable
            grabbableComponent.enabled = grabbable;
          //  Debug.Log($"Grabbable component is now {(grabbable ? "enabled" : "disabled")}");
        }
        else
        {
           // Debug.LogWarning("Grabbable component not found!");
        }

        //  }

        //////////isgrabbed verefy 
        IsGrabbed isGrabbedComponent = GetComponent<IsGrabbed>();
        if (isGrabbedComponent != null)
        {
            if (isGrabbedComponent.isGrabbed)
            {
                isgrabbed = true;
            }
            if (!isGrabbedComponent.isGrabbed)
            {
                isgrabbed = false;
            }

        }

        if (isgrabbed)
        {
            motherboardPlaced = false;
           
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!motherboardPlaced)
        {
            if (other.CompareTag("caseMotherboardH") && !isgrabbed)
            {
                Transform motherboardPlace = other.transform.parent.Find("motherboardPlace");

                if (motherboardPlace != null && !isgrabbed)
                {
                    rotationLimit = IsRotationWithinLimit(transform.rotation, motherboardPlace.rotation, 30f);


                  //  Debug.Log("no screw found " + nbScrew);
                    if (nbScrew == 0 && rotationLimit && installed == false)
                    {
                        motherboardPlaced = true;
                        motherboardDetect = other.transform;

                        Transform motherboardPlacee = motherboardDetect.parent.Find("motherboardPlace");
                        transform.position = motherboardPlace.position;
                        transform.rotation = motherboardPlace.rotation;
                        //StartCoroutine(SnapToCaseCoroutine(motherboardDetect));


                    }
                    if (!rotationLimit)
                    {
                     //   Debug.Log("Rotation difference too large. Cannot snap.");/////////////////////UI
                    }
                    if (nbScrew > 0)
                    {
                   //     Debug.Log("unscrew first please ");/////////////////UI
                    }

                }
            }
        }
    }

    private bool IsRotationWithinLimit(Quaternion currentRotation, Quaternion targetRotation, float limit)
    {
        Vector3 currentEulerAngles = currentRotation.eulerAngles;
        Vector3 targetEulerAngles = targetRotation.eulerAngles;

        float xDifference = Mathf.DeltaAngle(currentEulerAngles.x, targetEulerAngles.x);
        float yDifference = Mathf.DeltaAngle(currentEulerAngles.y, targetEulerAngles.y);
        float zDifference = Mathf.DeltaAngle(currentEulerAngles.z, targetEulerAngles.z);

        if (Mathf.Abs(xDifference) <= limit && Mathf.Abs(yDifference) <= limit && Mathf.Abs(zDifference) <= limit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
