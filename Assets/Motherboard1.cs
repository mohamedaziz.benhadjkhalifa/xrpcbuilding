using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motherboard1 : MonoBehaviour
{

    public bool grabbable = true;
    private Transform motherboardDetect;
    public bool rotationLimit = false;
    //public  bool noScrew = true;
    public bool motherboardPlaced = false;

    public bool MotherboardInstalled = false;
    public bool isGrabbed = false;

    Transform motherboardPlace;

    public int nbScrew = 0;

    private Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {


        var grabbableComponent = GetComponent<GrabInteractable>();

        if (grabbableComponent != null)
        {
            // Activate or deactivate the component based on the value of grabbable
            grabbableComponent.enabled = grabbable;
         //   Debug.Log($"Grabbable component is now {(grabbable ? "enabled" : "disabled")}");
        }
        else
        {
         //   Debug.LogWarning("Grabbable component not found!");
        }

        //////////isgrabbed verefy 
        IsGrabbed isGrabbedComponent = GetComponent<IsGrabbed>();
        if (isGrabbedComponent != null)
        {
            if (isGrabbedComponent.isGrabbed)
            {
                isGrabbed = true;
            }
            if (!isGrabbedComponent.isGrabbed)
            {
                isGrabbed = false;
            }

        }



        if (MotherboardInstalled)
        {
            grabbable = false;
        }
        if (!MotherboardInstalled)
        {
            grabbable = true;
        }

        if (isGrabbed)
        {
           
            transform.parent = null;
            rb.isKinematic = true;
            motherboardPlaced = false;
            MotherboardInstalled = false; 

        }
        if (!isGrabbed && !motherboardPlaced )
        {
            rb.isKinematic = false;
        }
        if ( MotherboardInstalled)
        {
            rb.isKinematic = true;
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("caseMotherboardH") && !isGrabbed)
        {
            if (!motherboardPlaced)
            {

                if (nbScrew == 0)
                {

                    motherboardPlace = other.transform.parent.Find("motherboardPlace");


                  //  Debug.Log("motherboard placed :" +  motherboardPlaced);
                    transform.position = motherboardPlace.position;
                    transform.rotation = motherboardPlace.rotation;
                    motherboardPlaced = true;
                }


            }
            if (motherboardPlaced)
            {
                //Debug.Log("motherboard placed 2:" + motherboardPlaced);
                if (nbScrew > 0)
                {

                    motherboardPlace = other.transform.parent.Find("motherboardPlace");


                    transform.position = motherboardPlace.position;
                    transform.rotation = motherboardPlace.rotation;

                    transform.parent = motherboardPlace;
                    MotherboardInstalled = true;
                    
                }
                if (nbScrew == 0)
                {

                    motherboardPlace = other.transform.parent.Find("motherboardPlace");


                    transform.position = motherboardPlace.position;
                    transform.rotation = motherboardPlace.rotation;

                    transform.parent = motherboardPlace;
                    MotherboardInstalled = false;

                }


            }


        }


    }

    private bool IsRotationWithinLimit(Quaternion currentRotation, Quaternion targetRotation, float limit)
    {
        Vector3 currentEulerAngles = currentRotation.eulerAngles;
        Vector3 targetEulerAngles = targetRotation.eulerAngles;

        float xDifference = Mathf.DeltaAngle(currentEulerAngles.x, targetEulerAngles.x);
        float yDifference = Mathf.DeltaAngle(currentEulerAngles.y, targetEulerAngles.y);
        float zDifference = Mathf.DeltaAngle(currentEulerAngles.z, targetEulerAngles.z);

        if (Mathf.Abs(xDifference) <= limit && Mathf.Abs(yDifference) <= limit && Mathf.Abs(zDifference) <= limit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
