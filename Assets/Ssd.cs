using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ssd : MonoBehaviour
{

    public bool isGrabbed = false;
    private Rigidbody rb;
    public bool inPlace = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        IsGrabbed isGrabbedComponent = GetComponent<IsGrabbed>();
        if (isGrabbedComponent != null)
        {
            if (isGrabbedComponent.isGrabbed)
            {
                isGrabbed = true;
            }
            if (!isGrabbedComponent.isGrabbed)
            {
                isGrabbed = false;
            }

        }

        if (isGrabbed)
        {

            transform.parent = null;
            rb.isKinematic = true;
        }
        if (!isGrabbed && !inPlace)
        {
            transform.parent = null;
            rb.isKinematic = false;

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("SsdPlace") && !isGrabbed)
        {

            if (transform.parent == null)
            {

                bool hasRamChild = false;
                foreach (Transform child in other.transform)
                {
                    if (child.CompareTag("ssd"))
                    {
                        hasRamChild = true;
                        break;
                    }
                }

                if (!hasRamChild && !isGrabbed)
                {
                    // Snap to RamPlace position
                    transform.position = other.transform.position;
                    transform.rotation = other.transform.rotation;

                    // Make the RAM a child of RamPlace
                    transform.SetParent(other.transform);
                    inPlace = true;
                    rb.isKinematic = true;

                }
            }
        }
    }
}
