using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YaxesTranslator : MonoBehaviour
{
    public float rotationSpeed = 100f; // Speed at which the object rotates
    public float moveSpeed = 0.1f; // Speed at which the object moves up and down
    private float previousYRotation;

    void Start()
    {
        // Initialize the previousYRotation with the initial Y rotation of the object
        previousYRotation = transform.eulerAngles.y;
    }

    void Update()
    {
        // Get the current Y rotation of the object
        float currentYRotation = transform.eulerAngles.y;

        // Calculate the change in rotation
        float deltaYRotation = currentYRotation - previousYRotation;

        // If the change in rotation is significant
        if (Mathf.Abs(deltaYRotation) > 0.01f)
        {
            // Determine the direction of movement
            float direction = Mathf.Sign(deltaYRotation);

            // Move the object up or down based on the rotation direction
            transform.Translate(Vector3.down * direction * moveSpeed * Time.deltaTime);

            // Update the previousYRotation for the next frame
            previousYRotation = currentYRotation;
        }

        // Rotate the object around its Y-axis based on player input
        float rotationInput = Input.GetAxis("Horizontal"); // Assuming "Horizontal" axis is used for rotation
        transform.Rotate(Vector3.up, rotationInput * rotationSpeed * Time.deltaTime);
    }
}
