using Oculus.Interaction;
using Oculus.Interaction.Editor;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class screw1 : MonoBehaviour
{
    public Transform screwHole;

    public bool isAttached = false;
    public bool isScrued = false;
    private Rigidbody rb;

    public float transitionSpeed = 1.0f;
    public float rotationSpeed = 1.0f;

    public bool screwing = false;

    public bool grabbable = true;

    public bool isGrabbed = false;

    public Transform screwPos1 = null;
    public Transform screwPos2 = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("screwHole") && !isGrabbed)
        {
            if (!isAttached)
            {
                isAttached = true;

                screwHole = other.transform;
                bool reserved = false;
                foreach (Transform child in screwHole.parent)
                {
                    if (child.CompareTag("screw"))
                    {
                        reserved = true;
                        break; // Exit the loop once the child is found
                    }
                }
                if (!reserved)
                {
                    foreach (Transform child in screwHole.parent)
                    {
                        if (child.CompareTag("screwPos1"))
                        {
                            screwPos1 = child;
                            break; // Exit the loop once the child is found
                        }
                    }
                    foreach (Transform child in screwHole.parent)
                    {
                        if (child.CompareTag("screwPos2"))
                        {
                            screwPos2 = child;
                            break; // Exit the loop once the child is found
                        }
                    }
                    // screwPos1 = screwHole.parent.Find("screwPos1");

                //    Debug.Log("screwPos names :" + screwPos1);

                    transform.position = screwPos1.position;
                    transform.rotation = screwPos1.rotation;

                    transform.parent = screwHole.parent;

                    if (rb != null)
                    {
                        rb.isKinematic = true;
                    }
                    if (GetComponent<CapsuleCollider>().isTrigger == false)
                    {
                        GetComponent<CapsuleCollider>().isTrigger = true;
                    }


                }
            }




        }
        if (other.CompareTag("ScrewDriver") && isAttached  && !screwing)
        {
            Motherboard1 motherboardScript = screwHole.parent.parent.GetComponent<Motherboard1>();

            if (isScrued)
            {
                //  motherboardScript.scrued = false;
                motherboardScript.nbScrew--;
                StartCoroutine(MoveToPosition(screwPos1.position, screwPos1.rotation));

                // StartCoroutine(screwCouroutineB(screwHole));
            }
            else if (!isScrued)
            {
                motherboardScript.nbScrew++;
                StartCoroutine(MoveToPosition2(screwPos2.position, screwPos2.rotation));
            }

        }



    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("screwHole") && isGrabbed )
        {
          //  Debug.Log("screw triggerExit");
            GetComponent<CapsuleCollider>().isTrigger = false;

        }

    }
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        //////////isgrabbed verefy 
        IsGrabbed isGrabbedComponent = GetComponent<IsGrabbed>();
        if (isGrabbedComponent != null)
        {
            if (isGrabbedComponent.isGrabbed)
            {
                isGrabbed = true;
            }
            if (!isGrabbedComponent.isGrabbed)
            {
                isGrabbed = false;
            }

        }

        if (isGrabbed)
        {
          //  Debug.Log("grabbed");
           // GetComponent<CapsuleCollider>().isTrigger = false;
            isAttached = false;
            transform.parent = null;
            rb.isKinematic = true;
            screwPos1 = null;
            screwPos2 = null;
            screwHole = null;

}

        if (!isGrabbed && !isAttached)
        {
           // Debug.Log("nograb noattached");
            rb.isKinematic = false;
        }

        var grabbableComponent = GetComponent<GrabInteractable>();

        if (grabbableComponent != null)
        {
            // Activate or deactivate the component based on the value of grabbable
            grabbableComponent.enabled = grabbable;
           // Debug.Log($"Grabbable component is now {(grabbable ? "enabled" : "disabled")}");
        }
        else
        {
           // Debug.LogWarning("Grabbable component not found!");
        }

        if (isScrued)
        {
            grabbable = false;
        }
        if (!isScrued)
        {
            grabbable = true;
        }
    }


    private IEnumerator MoveToPosition(Vector3 targetPosition, Quaternion targetRotation)
    {
        screwing = true;
        while (Vector3.Distance(transform.position, targetPosition) > 0.01f || Quaternion.Angle(transform.rotation, targetRotation) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, transitionSpeed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            yield return null;
        }

        // Ensure final position and rotation are set correctly
        transform.position = targetPosition;
        transform.rotation = targetRotation;


        screwing = false;
        // Additional actions can be added here
        isScrued = false ; // Toggle the state
    }
    private IEnumerator MoveToPosition2(Vector3 targetPosition, Quaternion targetRotation)
    {
        screwing = true;
        while (Vector3.Distance(transform.position, targetPosition) > 0.01f || Quaternion.Angle(transform.rotation, targetRotation) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, transitionSpeed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            yield return null;
        }

        // Ensure final position and rotation are set correctly
        transform.position = targetPosition;
        transform.rotation = targetRotation;


        screwing = false;
        // Additional actions can be added here
        isScrued = true; // Toggle the state
    }
}
