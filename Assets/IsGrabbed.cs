using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsGrabbed : MonoBehaviour
{
    private Grabbable _grabbable;
    public bool isGrabbed { get; private set; }

    private void Awake()
    {
        _grabbable = GetComponent<Grabbable>();

        if (_grabbable == null)
        {
           // Debug.LogError("Grabbable component not found on the GameObject.");
            enabled = false;
            return;
        }
    }

    private void OnEnable()
    {
        _grabbable.WhenPointerEventRaised += HandlePointerEvent;
    }

    private void OnDisable()
    {
        _grabbable.WhenPointerEventRaised -= HandlePointerEvent;
    }

    private void HandlePointerEvent(PointerEvent evt)
    {
        switch (evt.Type)
        {
            case PointerEventType.Select:
                isGrabbed = true;
                break;
            case PointerEventType.Unselect:
            case PointerEventType.Cancel:
                isGrabbed = false;
                break;
        }
    }

    private void Update()
    {
       // Debug.Log($"isGrabbed: {isGrabbed}");
    }
}
